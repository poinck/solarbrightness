

default:
	@echo "type \"make install\" as root to install solarbrightness components"

install:
	ln -sf ${PWD}/solarbrightness ${HOME}/bin/sbri
	install -D -m 644 ${PWD}/config/systemd/system/solarbrightness.timer /etc/systemd/system/solarbrightness.timer
	install -D -m 644 ${PWD}/config/systemd/system/solarbrightness.service /etc/systemd/system/solarbrightness.service
	@echo "you can now enable the \"solarbrightness.timer\""


# Readme: solarbrightness

**solarbrightness (sbri):**
Sets brightness using the sys-kernel-interface depending on solar elevation provided by *"redshift"*

## Dependencies
- redshift
- systemd

## Clone, Configure and Compile
```.sh
cd /home/$USER
mkdir gits
cd gits
git clone https://gitlab.com/poinck/solarbrightness
cd solarbrightness

# as root
make install
systemctl enable solarbrightness.timer
```

## Configuration
copy "config/.solarbrightnessrc" to your home-directory if you want to change the defaults

##
